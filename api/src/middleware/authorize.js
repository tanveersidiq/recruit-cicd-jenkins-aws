var jwt = require('express-jwt'),
  jwks = require('jwks-rsa');

var authorize = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: process.env.AUTH0_ISSUER + ".well-known/jwks.json"
  }),
  issuer: process.env.AUTH0_ISSUER,
  algorithms: ['RS256']
});

module.exports = authorize;