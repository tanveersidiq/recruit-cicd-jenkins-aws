module.exports = function (models, router) {
    var candidateRouter = router.Router();
    candidateRouter
        .get('/candidate', function (req, res) {
            models
                .Users
                .findOne({
                    where: {
                        'username': req.query.username
                    },
                    required: true,
                    include: [{
                        model: models.Candidates,
                        as: 'Candidates',
                        required: false
                    }]
                })
                .then(function (user) {
                    res
                        .json(user);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notFound(error);
                });
        })
        .post('/candidate', function (req, res) {
            
            let candidate = req.body;
            models
                .Candidates
                .findOne({
                    where: {
                        'user': candidate.user
                    }
                })
                .then(function (data) {
                    if (data) {
                        models
                            .Candidates
                            .update(candidate, {
                                where: {
                                    email: candidate.email
                                }
                            })
                            .then(function (data) {
                                res
                                    .json(candidate);
                            })
                            .catch(function (error) {
                                res
                                    .boom
                                    .badData(error);
                            });
                    } else {
                        models
                            .Candidates
                            .create(candidate)
                            .then(function (candidate) {
                                res
                                    .json(candidate);
                            })
                            .catch(function (error) {
                                res
                                    .boom
                                    .badData(error);
                            });
                    }

                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });

        });

    return candidateRouter;
};