module.exports = function (models, router) {
    var companyRouter = router.Router();
    companyRouter
        .get('/companies', function (req, res) {
            models.Companies.findAll({
                    order: 'Company_ID ASC'
                })
                .then(function (companies) {
                    res.status(200).json(companies);
                })
                .catch(function (error) {
                    res.status(500).json(error);
                });
        });
    return companyRouter;
};