var routes = function (models, router) {

    var userRouter = router.Router();

    userRouter
        .get('/user:username', function (req, res) {
            models
                .Users
                .findOne({
                    where: {
                        'username': req.params.username
                    }
                })
                .then(function (user) {
                    res
                        .json(user);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notFound(error);
                });
        })
        .post('/user', function (req, res) {
            let user = req.body;
            models
                .Users
                .findOne({
                    where: {
                        'username': user.username
                    },
                    required: true,
                    include: [{
                        model: models.Candidates,
                        as: 'Candidates',
                        required: false
                    }]
                })
                .then(function (data) {
                    if (data) {
                        user.id = data.id;
                        models
                            .Users
                            .update(user, {
                                where: {
                                    'username': user.username
                                }
                            })
                            .then(function (data) {
                                res
                                    .json(user);
                            })
                            .catch(function (error) {
                                res
                                    .boom
                                    .badData(error);
                            });
                    } else {
                        models
                            .Users
                            .create(user)
                            .then(function (response) {
                                res
                                    .json(response);
                            })
                            .catch(function (error) {
                                res
                                    .boom
                                    .badData(error);
                            });
                    }

                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });

        });

    return userRouter;
};

module.exports = routes;