var models = require('../models/index.js'),
    authorize = require('../middleware/authorize.js');

module.exports = function (app, express, routeStart) {

    app.use(routeStart, authorize, require('./userRoutes')(models, express));
    app.use(routeStart, authorize, require('./companyRoutes')(models, express));    
    app.use(routeStart, authorize, require('./candidateRoutes')(models, express));    
    app.use('/', function (req, res) {
        res.send('Recruit Sync 2 API is at http://' + req.headers.host + '/api');
    });

}