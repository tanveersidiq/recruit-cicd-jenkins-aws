'use strict';
module.exports = function (sequelize, DataTypes) {
    var Candidate = sequelize
        .define('Candidates', {
            id: {
                field: 'Id',
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            firstName: {
                field: 'FirstName',
                type: DataTypes.STRING
            },
            lastName: {
                field: 'LastName',
                type: DataTypes.STRING
            },
            dateOfBirth: {
                field: 'DateOfBirth',
                type: DataTypes.DATE
            },
            address: {
                field: 'Address',
                type: DataTypes.TEXT
            },
            city: {
                field: 'City',
                type: DataTypes.STRING
            },
            state: {
                field: 'State',
                type: DataTypes.STRING
            },
            zip: {
                field: 'Zip',
                type: DataTypes.STRING
            },
            email: {
                field: 'Email',
                type: DataTypes.STRING
            },
            mobile: {
                field: 'Mobile',
                type: DataTypes.STRING
            },
            phone: {
                field: 'Phone',
                type: DataTypes.STRING
            },
            classOf: {
                field: 'ClassOf',
                type: DataTypes.STRING
            },
            underGraduate: {
                field: 'UnderGraduate',
                type: DataTypes.STRING
            },
            lawSchool: {
                field: 'LawSchool',
                type: DataTypes.STRING
            },
            user: {
                field: '[User]',
                type: DataTypes.STRING
            }
        }, {
            timestamps: false
        });

    return Candidate;
};