'use strict';
module.exports = function (sequelize, DataTypes) {
    var User = sequelize
        .define('Users', {
            id: {
                field: 'Id',
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                field: 'Username',
                type: DataTypes.STRING,
                allowNull: false
            },
            pictureURL: {
                field: 'PictureURL',
                type: DataTypes.STRING
            }
        }, {
            timestamps: false
        });

    return User;
};