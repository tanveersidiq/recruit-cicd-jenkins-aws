# Recruit Sync

Recruit Sync is used for organizations sharing the expenditures of shared associates or employees. Test

## What is this repository for?

* This repostiory will contain web and api projects of Recruit Sync which will be developed using Node.js and Angular 4.0
* 1.0

## How do I set up Recruit Sync?

* Install Nodejs
* Install git client
* Open shell or command prompt
* npm install -g @angular/cli
* git clone https://{yourusername}@bitbucket.org/ultramar/recruitsync.git
* Change working directory to cloned repository
* git fetch && git checkout dev

## How do I set up Recruit Sync api?

* Change working directory to cloned repository
* Change directory to api
* npm install
* Set API Environment Variables by modifying the .env file under api/src/environment folder

## How do I set up Recruit Sync web?

* Change working directory to cloned repository
* Change directory to web
* npm install
* Edit auth.config.ts file under web/src/environements with appropriate values

## Running api application

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to api
* gulp
* Navigate to http://localhost:{API_PORT}/
* The api will automatically reload if you change any of the source files.

## Running web application

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to web
* npm start
* Navigate to http://localhost:{PORT}/
* The application will automatically reload if you change any of the source files.

## Running web unit tests

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to web
* npm run test
* Executing unit tests via [Karma](https://karma-runner.github.io).

## Running web end to end tests

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to web
* npm start
* npm run e2e
* Executing end-to-end tests via [Protractor](http://www.protractortest.org/).

## Building web application for development stage

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to web
* npm run build
* The build artifacts will be stored in the `dist/` directory

## Build web application for production stage

* Open shell or command prompt
* Change working directory to cloned repository
* Change directory to web
* npm run build -prod
* The build artifacts will be stored in the `dist/` directory