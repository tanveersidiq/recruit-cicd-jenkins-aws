interface Auth0Configuration {
  clientID: string,
  domain: string,
  redirectUri: string
}

export const auth0Config: Auth0Configuration = {
  clientID:'jWtVal26ZytI07NpNdI34x7aa9eflq5a',
  domain: 'tanveersidiq.auth0.com',
  redirectUri: 'http://localhost:5000'
};

interface APIConfiguration {
  apiUri: string
}

export const apiConfig: APIConfiguration = {
  apiUri: 'http://localhost:5001/api/'
};
