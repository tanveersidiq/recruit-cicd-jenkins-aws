import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { RecruitSyncModule } from './app/recruit-sync.module';

import {
  SecurityService,
  RouteGuardService
} from './app/recruit-sync.services';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(RecruitSyncModule);
