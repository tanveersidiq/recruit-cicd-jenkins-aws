import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'token',
    tokenGetter: (() => localStorage.getItem('id_token')),
    globalHeaders: [{ 'Content-Type': 'application/json' }],
  }), http, options);
}

import {
  RecruitSyncComponent,
  CallbackComponent,
  SigninComponent,
  SignupComponent,
  CandidateDetailComponent
} from './recruit-sync.components';

import {
  RouteGuardService,
  SecurityService
} from './recruit-sync.services';

import {
  routing,
  appRoutingProviders
} from './recruit-sync.routes';

@NgModule({
  declarations: [
    RecruitSyncComponent,
    CallbackComponent,
    SigninComponent,
    SignupComponent,
    CandidateDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
    RouteGuardService,
    SecurityService,
    appRoutingProviders,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }
  ],
  bootstrap: [
    RecruitSyncComponent
  ]
})
export class RecruitSyncModule { }
