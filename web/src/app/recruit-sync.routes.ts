import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  RecruitSyncComponent,
  SigninComponent,
  SignupComponent,
  CandidateDetailComponent
} from './recruit-sync.components';

import {
  RouteGuardService
} from './recruit-sync.services';

const appRoutes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'candidatedetail', component: CandidateDetailComponent, canActivate: [RouteGuardService]  },
  { path: '**', redirectTo: 'signin' }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
