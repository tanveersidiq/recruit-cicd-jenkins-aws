import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../../common/services/authentication/security.service';
import { CandidateService } from '../service/candidate.service';
import {
    User,
    Candidate
} from '../../recruit-sync.models';
@Component({
    selector: 'candidate-detail',
    templateUrl: './candidate-detail.component.html',
    styleUrls: ['./candidate-detail.component.css'],
    providers: [
        CandidateService
    ]
})
export class CandidateDetailComponent implements OnInit {

    user: User;
    candidate: Candidate;

    constructor(
        private authenticationService: SecurityService,
        private candidateService: CandidateService) {
        this.candidate = new Candidate();
    }

    ngOnInit() {

        this.user = this.authenticationService.user;
        this.candidate = this.authenticationService.candidate;
        this
            .candidateService
            .get(this.user.username)
            .subscribe(user => {
                if (user) {
                    if (user.Candidates && user.Candidates.length > 0) {
                        this.candidate = user.Candidates[0];
                    }
                }

            });
    }

}
