import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/map';

import { apiConfig } from 'config/global';

@Injectable()
export class CandidateService {

  users: any;
  constructor(private http: AuthHttp) { }

  public get(username) {
    return this
      .http
      .get(apiConfig.apiUri + 'candidate?username=' + username)
      .map(data => data.json());
  }

  public save(candidate) {
    return this
      .http
      .post(apiConfig.apiUri + 'candidate', candidate)
      .map(data => data.json());
  }

}
