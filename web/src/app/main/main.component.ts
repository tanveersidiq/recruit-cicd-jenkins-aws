import { Component } from '@angular/core';
import { SecurityService } from '../common/services/authentication/security.service';
@Component({
  selector: 'recruit-sync',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class RecruitSyncComponent {
  constructor(private authentication: SecurityService) {
    this.authentication.handleAuthentication();
  }
}
