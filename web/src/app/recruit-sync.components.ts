export * from './main/main.component';
export * from './callback/callback.component';
export * from './login/signin/signin.component';
export * from './login/signup/signup.component';
export * from './candidate/detail/candidate-detail.component';
