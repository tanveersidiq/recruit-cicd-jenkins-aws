export class Candidate {
  firstName: String;
  lastName: String;
  email: String;
  phone: String;
  mobile: String;
  address: String;
  lawSchool: String;
  classOf: String;
  underGraduate: String;
  user: Number;
}
