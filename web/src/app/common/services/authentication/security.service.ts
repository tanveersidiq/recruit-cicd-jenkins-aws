import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';
import { auth0Config, apiConfig } from '../../../../config/global';
import { User, Candidate } from '../../../recruit-sync.models';
import { AuthHttp } from 'angular2-jwt';

declare var auth0: any; // Avoid name not found warnings
@Injectable()
export class SecurityService {

  auth0 = new auth0.WebAuth({
    domain: auth0Config.domain,
    clientID: auth0Config.clientID,
    redirectUri: auth0Config.redirectUri,
    responseType: 'token id_token'
  });

  user = new User();
  candidate = new Candidate();

  constructor(private router: Router, private http: AuthHttp) {
  }

  public handleAuthentication(): void {

    let options = { _idTokenVerification: false };

    this
      .auth0
      .parseHash(options, (error, response) => {
        if (error) {
          alert(`Error: ${error.errorDescription}`)
        }
        else if (response && response.accessToken && response.idToken) {

          localStorage.setItem('access_token', response.accessToken);
          localStorage.setItem('id_token', response.idToken);

          this
            .auth0
            .client
            .userInfo(response.accessToken, (error, response) => {
              if (error) {
                alert(error);
              }
              else {

                response.user_metadata = response.user_metadata || {};

                localStorage
                  .setItem('profile', JSON.stringify(response));

                this.user = new User();
                this.user.username = response.user_id;
                this.user.pictureURL = response.picture_large || response.picture;

                this.candidate.email = response.email || '';
                this.candidate.firstName = response.given_name ||  response.givenName ||'';
                this.candidate.lastName = response.family_name || response.familyName ||'';

                window.location.hash = '';
                this.router.navigate(['/candidatedetail']);

                this
                  .http
                  .post(apiConfig.apiUri + 'user', this.user)
                  .subscribe(response => {
                    let user = response.json();
                    if (user && !user.Candidates) {
                      this.candidate.user = user.id;
                      this
                        .http
                        .post(apiConfig.apiUri + 'candidate', this.candidate)
                        .subscribe(response => response.json());
                    }

                  });
              }
            });

        }
      });

  }

  public login(username: string, password: string): void {

    this.auth0.redirect.loginWithCredentials({
      connection: 'Username-Password-Authentication',
      username,
      password
    }, err => {
      if (err) return alert(err.description);
    });
  }

  public signup(email: string, password: string): void {
    this.auth0.redirect.signupAndLogin({
      connection: 'Username-Password-Authentication',
      email,
      password,
    }, err => {
      if (err) return alert(err.description);
    });
  }

  public googleLogin(): void {
    this.auth0.authorize({
      connection: 'google-oauth2',
    });
  }

  public yahooLogin(): void {
    this.auth0.authorize({
      connection: 'yahoo',
    });
  }

  public isAuthenticated(): boolean {
    return tokenNotExpired('id_token');
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    this.router.navigate(['/']);
  }

  private setUser(authResult): void {
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
  }

}
