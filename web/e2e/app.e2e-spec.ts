import { RecruitSyncWebPage } from './app.po';

describe('Recruit Sync web Application', () => {
  let page: RecruitSyncWebPage;

  beforeEach(() => {
    page = new RecruitSyncWebPage();
  });

  it("should display navigation brand as Recruit Sync", () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Recruit Sync');
  });
});
